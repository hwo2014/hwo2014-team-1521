/*
 * Car.h
 *
 *  Created on: 15 Apr 2014
 *      Author: wybourn
 */

#ifndef CAR_H_
#define CAR_H_

#include "Vehicle.h"

class Car : public Vehicle
{
public:
  Car ();
  ~Car ();

};

#endif /* CAR_H_ */
