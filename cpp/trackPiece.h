/*
 * trackPiece.h
 *
 *  Created on: 17 Apr 2014
 *      Author: wybourn
 */

#ifndef TRACKPIECE_H_
#define TRACKPIECE_H_

typedef struct trackPiece
{
  float length;
  float radius;
  float angle;
  bool  corner;
}trackPiece;


#endif /* TRACKPIECE_H_ */
