/*
 * TrackParser.cpp
 *
 *  Created on: 18 Apr 2014
 *      Author: wybourn
 */

#include "TrackParser.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

TrackParser::TrackParser ()
{
  // TODO Auto-generated constructor stub
}

TrackParser::~TrackParser ()
{
  // TODO Auto-generated destructor stub
}

string TrackParser::getTrackNameFromJson(const jsoncons::json& data)
{
  std::stringstream ss(data.to_string());

  boost::property_tree::ptree ptree;
  boost::property_tree::read_json(ss, ptree);

  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, ptree.get_child("race.track"))
  {
    if(v.first == "id")
    {
      return v.second.data();
    }
  }

  return "";
}

vector<trackPiece> TrackParser::getTrackPiecesFromJson(const jsoncons::json& data)
{
  stringstream ss(data.to_string());

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(ss, pt);

  using boost::property_tree::ptree;
  ptree::const_iterator it = pt.begin()->second.begin();
  it++; it++;

  for(it = it->second.begin(); it->first != "pieces"; it++)
  {
    ;
  }

  vector<trackPiece> pieces;

  ptree::const_iterator end = it->second.end();

  for(it = it->second.begin();it != end;it++)
  {
    ptree::const_iterator pit = it->second.begin();
    trackPiece piece = {0};

    if(pit->first == "length")
    {
      piece.length = pit->second.get_value<float>();
    }
    else if(pit->first == "angle")
    {
      piece.angle = pit->second.get_value<float>();
      piece.corner = true;
    }

    pieces.push_back(piece);
  }

  return pieces;
}

int TrackParser::getCurrentPieceIndex(const jsoncons::json& data)
{
  return 0;
}



