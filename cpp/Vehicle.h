/*
 * Vehicle.h
 *
 *  Created on: 15 Apr 2014
 *      Author: wybourn
 */

#ifndef VEHICLE_H_
#define VEHICLE_H_

#include <string>

using namespace std;

class Vehicle
{
public:
  const string getColor(void);
protected:
  void setColor(string& vehicleColor);

  string color;
};

#endif /* VEHICLE_H_ */
