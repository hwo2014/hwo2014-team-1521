/*
 * track.h
 *
 *  Created on: 15 Apr 2014
 *      Author: wybourn
 */

#ifndef TRACK_H_
#define TRACK_H_

#include <vector>
#include <string>
#include "trackPiece.h"

using namespace std;

class Track
{
public:
  Track ();
  virtual ~Track ();

  void setTrackPieces(vector<trackPiece>& trackPieces);
  void setTrackId(string trackId);

private:
  string id = "";
  vector<trackPiece> pieces;
};

#endif /* TRACK_H_ */
