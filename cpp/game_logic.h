#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <vector>
#include "trackPiece.h"
#include "protocol.h"
#include "TrackParser.h"

typedef std::vector<jsoncons::json> msg_vector;

using namespace std;

class game_logic
{
public:
  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const map<string, action_fun> action_map;
  TrackParser* trackParser;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_lap_finished(const jsoncons::json& data);
  msg_vector on_finish(const jsoncons::json& data);
  msg_vector on_tournament_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
};

#endif
