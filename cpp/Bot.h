/*
 * Bot.h
 *
 *  Created on: 15 Apr 2014
 *      Author: wybourn
 */

#ifndef BOT_H_
#define BOT_H_

#include "Vehicle.h"
#include "Track.h"
#include <vector>

using namespace std;

class Bot
{
public:
  Bot ();
  virtual ~Bot ();

  void setTrackPieces(vector<trackPiece>);
  void setTrackName(string name);

private:
  Vehicle* vehicle = NULL;
  Track* track;

};

#endif /* BOT_H_ */
