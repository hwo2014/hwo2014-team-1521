/*
 * TrackParser.h
 *
 *  Created on: 18 Apr 2014
 *      Author: wybourn
 */

#ifndef TRACKPARSER_H_
#define TRACKPARSER_H_

#include <string>
#include <vector>

#include "protocol.h"
#include "trackPiece.h"

using namespace std;

class TrackParser
{
public:
  TrackParser ();
  virtual ~TrackParser ();

  string getTrackNameFromJson(const jsoncons::json& data);
  vector<trackPiece> getTrackPiecesFromJson(const jsoncons::json& data);
  int getCurrentPieceIndex(const jsoncons::json& data);
};

#endif /* TRACKPARSER_H_ */
