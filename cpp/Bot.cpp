/*
 * Bot.cpp
 *
 *  Created on: 15 Apr 2014
 *      Author: wybourn
 */

#include "Bot.h"
#include "Car.h"

Bot::Bot ()
{
  vehicle = new Car();
  track = new Track();
}

Bot::~Bot ()
{
  // TODO Auto-generated destructor stub
}

void Bot::setTrackName(string name)
{
  track->setTrackId(name);
}

void Bot::setTrackPieces(vector<trackPiece> trackPieces)
{
  track->setTrackPieces(trackPieces);
}
